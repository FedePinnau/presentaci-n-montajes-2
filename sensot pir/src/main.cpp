#include <Arduino.h>

#define PIR  2
#define LED  3
bool estado = LOW;
void setup() {
  pinMode(LED,OUTPUT);
  pinMode(PIR,INPUT);
  attachInterrupt(digitalPinToInterrupt(PIR),Interrupcion,RISING);
  Serial.begin(9600);
}

void loop() {
  if(estado == HIGH){
    digitalWrite(LED,HIGH); 
    delay (1000);
    estado = LOW;
    Serial.println("No hay movimiento");//imprime que no hay movimiento
  }
}
void Interrupcion(){
  estado = HIGH;
  Serial.println("Hay movimiento");//imprime que hay movimiento
}